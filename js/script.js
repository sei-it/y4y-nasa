

$(document).ready(function() {


	$('.switcher li a').on('click', function (ev) {
		var id = $(this).attr('id');

		$('.switcher li a').removeClass('active');
		$(this).addClass('active');
		// toggle view
		$('#frame').removeClass('portrait landscape desktop').addClass(id);

		
		var html = $('html');
		html.removeClass('desktop');
		if (id === 'desktop') {
		
			if (html.hasClass('desktop')) {
				html.removeClass('desktop');
			} else	{
				html.addClass('desktop');
			}
		}

//		$('.overthrow').overthrow.set();

		return false;
	});


//	send variable to iframe
//	var url = window.location.search;
//	$('frame').attr('src', 'index.php' + url);

//	var update_url = function(link, ev) {
//		var history_api = !!(window.history && history.pushState);
//		var title = ev.target.textContent;  currently does not work
//		if (history_api) {
//			history.pushState(null, title, link);
//		} else {
//			window.location.href = link;
//		}
//	};

	// smartphones

	if (screen.width < 480) {
//		$('#navbar').hide();
		$('#frame').removeClass('overthrow');
		$('body').addClass('small-screen ');

		// jump to content
		setTimeout(function() { window.scrollTo(0, 45); }, 100);
	}
	
/*
	$(window).resize(function () {
		if ($(document).width() >= 768) {

		}
	});
*/

/*  ========================
	PATTERNS
	======================== */



/* !===== INFINITE LIST ===== */

$.ajaxSetup({
	cache: false
});
$('.more').on('click', function () {
	console.log(this);
	$.ajax({
		url: "/prototype",
		data: 'p=infinite-list',
		success: function (html) {
			var ul = $(html).find('.list');
			$('.list').append(ul);
		}
	});
});
/* !===== EXPANDING LIST ===== */

$('.expanding-list a').on('click', function (ev) {
	$(this).toggleClass('active').next().slideToggle();
	ev.preventDefault();
});

/* !===== TOGGLE MENU ===== */

/* $('.collapsed').hide(); */
$('.toggle-button').on('click', function () {
	$(this).toggleClass('active');
	$('.collapsed').slideToggle();
});


/* !===== SIDE MENU ===== */
$('.toggle-side').on('click', function () {
//	$('#side').toggle();
	$('#side').toggleClass('menu-active');
});


/* !===== SLIDESHOW ===== */

window.slider = new Swipe(document.getElementById('slider'));


/* !===== ICEBERG TIP ===== */

$('.iceberg-tip #show-target').on('click', function () {
	$('.iceberg-tip a').toggleClass('show-target');
});

$('.iceberg-tip .target a').on('click', function () {
	$(this).toggleClass('active');
});

/* ===== ABRIDGED TABLE ===== */

$('.mediaTable').mediaTable();


/* ===== DROPDOWN ===== */

//$('.dropdown-hidden').hide();
$('.dropdown-toggle').on('click', function () {
	$(this).parent().toggleClass('active');
});

//remove menu
$('body').on('click', function() {
//	$('.active').removeClass('active');
});

/* ===== DYNAMIC FILTERING ===== */

jQuery.expr[':'].Contains = function(a,i,m){
		return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
	};


function listFilter(header, list) { // header is any element, list is an unordered list
	// create and add the filter form to the header
	var form = $("<form>").attr({"class":"filterform","action":"#"}),
		input = $("<input>").attr({"class":"filterinput","type":"text"});
	$(form).append(input).appendTo(header);
	$(input)
	.change( function () {
		var filter = $(this).val();
		if(filter) {
		// this finds all links in a list that contain the input,
		// and hide the ones not containing the input while showing the ones that do
			$(list).find("li:not(:Contains(" + filter + "))").slideUp();
			$(list).find("li:Contains(" + filter + ")").slideDown();
			console.log(this);
		} else {
			$(list).find("li").slideDown();
		}
		return false;
	})
	.keyup( function () {
		// fire the above change event after every letter
		$(this).change();
	});
}

//ondomready
listFilter($("#filter"), $(".filter-list"));




$('.entry input[type=text]').on('keyup', function () {
	if ($('input[type=text]').val().length > 0) {
		$('.clear-button').fadeIn(200);
	} else {
		$('.clear-button').fadeOut(200);
	}
});
$('.clear-button').on('click', function () {
	$('input[type=text]').val('');
	$(this).fadeOut(200);
});

/* ===== SELECT MENU ===== */

	$("#nav-select").tinyNav();


// end
});


/* ===== TABS ==== */
/*
* Skeleton V1.1
* Copyright 2011, Dave Gamache
* www.getskeleton.com
* Free to use under the MIT license.
* http://www.opensource.org/licenses/mit-license.php
* 8/17/2011
*/


$('body').on('click', 'ul.tabs > li > a', function(e) {

    //Get Location of tab's content
    var contentLocation = $(this).attr('href');

    //Let go if not a hashed one
    if(contentLocation.charAt(0)==="#") {

        e.preventDefault();

        //Make Tab Active
        $(this).parent().siblings().children('a').removeClass('active');
        $(this).addClass('active');

        //Show Tab Content & add active class
        $(contentLocation).show().addClass('active').siblings().hide().removeClass('active');

    }
});
